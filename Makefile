DOCS = ruby-policy.pdf ruby-policy.html
PANDOC = pandoc --smart --number-sections --standalone --toc

build: $(DOCS)

%.html: %.mdw
	rm -rf $@
	$(PANDOC) -t html -o $@ $< 

%.pdf: %.mdw
	$(PANDOC) -o $@ $< 

clean:
	rm -rf $(DOCS)
