% Debian Ruby policy draft
% Vincent Fourmond <fourmond@debian.org>; Antonio Terceiro <terceiro@debian.org>; The Debian Ruby mailing list <debian-ruby@lists.debian.org>



Background
==========

There are several “subpolicies” in Debian. Their purpose is to provide
guidelines and packaging requirements in addition to the [Debian
Policy][debian-policy] when specific topics are concerned, such as
packages providing Perl libraries, Emacs extensions...

Similarly to the other subpolicies, the aim of the Debian Ruby Policy is
to unify the packaging of Ruby libraries and applications under a single
well-defined structure that packagers and users alike can rely on.

An important feature of Ruby is that there are quite a few different
implementations of the Ruby language, first of all the different versions
of the mainstream implementation (1.8 and 1.9.2), but also other
implementations, such as JRuby and Rubinius for instance. One of the
aims of this policy is to make it as painless as possible for different
versions of the Ruby language to coexist and to share code (in as far as
it is technically possible).

As packaging practices and Ruby-related technologies evolve over time,
this Policy is to some extent a permanent “work-in-progress”. Please
feel free to comment and suggest improvements on the
debian-ruby@lists.debian.org mailing list.

Policy
======

Only the present chapter contains the actual policy, i.e. normative
statements about how Ruby-related packages should be organized. The
verbs **must**, **must not**, **may** and **should** are used with the same
meanings as in the [Debian Policy][debian-policy].

Ruby interpreters {#interpreters}
---------------------------------

All Ruby interpreters **must** be installable at the same time.

The main executable **must** be placed in `/usr/bin` and match the name of
the principal binary package.

Interpreters **may** provide an alternatives entry for `/usr/bin/ruby`, so
that users can choose one of the different interpreter implementations
to provide the `ruby` program. This alternatives entry must have
slaves entries for the following programs: `erb`, `testrb`, `irb`,
`rdoc` and `ri`, as well as the corresponding manual pages for all of
those programs, including `ruby`.

Interpreter maintainers **may** use the `ruby1.8` and `ruby1.9.1` packages
as an example of the alternatives system handling for Ruby interpreters.

At the time of the writing of this policy, the default interpreter is
`ruby1.9.1`. This means that by default `/usr/bin/ruby` points to
`/usr/bin/ruby1.9.1`. Other interpreters **must** not install alternatives
entries with priority higher than `50` without first having a) a
consensus about that in the `debian-ruby` mailing list and b) a new
version of this policy uploaded to `unstable` pointing out a new default
interpreter.

Ruby interpreters **may** provide the `ruby-interpreter` virtual package, if
their maintainers consider that the implementation is mature enough to
support most Ruby software available.

In the case a Ruby interpreter provides the `ruby-interpreter` virtual
package, then it **must** depend on the `ruby-common` package.

### Load path

In addition to its own location for standard library code, all Ruby
interpreters **must** add the following directories to their library load
path:

-   `/usr/lib/ruby/vendor_ruby`, a location containing plain Ruby files;

-   `/usr/lib/ruby/vendor_ruby/`PKG, where PKG is the package name (and
    the interpreter name too), and/or a subdirectory of that directory
    whose name is architecture-dependant. This architecture-specific
    directory **must** be stored in the `RbConfig::CONFIG['vendorarchdir']`
    Ruby configuration variable.

The load paths **must** be searched for Ruby libraries in the following
order:

-   the *site directory*, where files are installed by local administrators
    (typically a subdirectory of `/usr/local`);

-   the aforementioned subdirectories in `/usr/lib/ruby/vendor_ruby/`;

-   and, finally, the system directories, where the system libraries
    bundled with the interpreter are installed (the Ruby standard
    library).

The aim of this setup is to allow the possibility for users and
packagers to override parts of the standard library.

Ruby libraries {#libraries}
---------------------------

By library, we mean “every package that contains Ruby code and is not a
Ruby interpreter”. In particular, this also includes applications not
necessarily meant to be used as libraries. For applications additional
requirements and/or relaxing of some requirements listed here apply, see
next section.

### Package names

Any binary package that contains Ruby code **must** have a package name in
the form `ruby-`PKG, where PKG **should** be the “library name”, that is
either the gem name or the argument to `require` (or the common part in
case of a library providing several sub-libraries that can be used
independently). However, packagers **may** ignore this rule in the following
cases:

-   Ruby interpreters, such as `ruby-1.9.1` or `jruby`;

-   applications, such as `chef`, `rails` or `rubygems`.

The source package name **should** match the name of the main binary
package.

In the case of software previously released with the old package name
policy (`lib-`PKG`-ruby` and versioned variants thereof), packagers **must**
provide transition packages with the old names depending on the new
package.

### Section

All libraries **must** be `Section:
        ruby`. This rule does not apply to applications, whose section
should match their use.

### Supported Ruby implementations

Packages **should** support as many different implementations of Ruby as is
possible. Reasons why this is not done **should** be documented visibly,
such as in the `README.Debian` file or the `changelog.Debian` file

Packages **must** list the versions of Ruby supported in the source package
using the `XS-Ruby-Versions` statement in `debian/control`. This
statement **must** be a space-separated list of one or more of `ruby1.8`,
`ruby1.9.1` or `all` (the latter meaning all the supported Ruby
versions).

All files for the different implementations **must** be shipped inside a
single binary package. This binary package **must** have a
`XB-Ruby-Versions` statement containing the space-separated list of Ruby
implementations supported by the binary package, with fields identical
to those for the source package, except that `all` cannot be used: the
full list **must** be listed explicitly.

### Installation paths

Pure Ruby code (i.e. Ruby files) **must** be installed under
`/usr/lib/ruby/vendor_ruby`.

Compiled extensions **must** be installed in the `'vendorarchdir'` path.

### Dependencies

All packages **must** depend either on 
  `ruby | ruby-interpreter`
if the package supports all implementations of Ruby or on the specific
implementations needed if that is not the case.

### Documentation

Packages **may** build a `rdoc` documentation. Unless the documentation is
really small, it **should** be packaged in a separate binary package named
`ruby-`PKG`-doc`.

### Rubygems

To avoid hard-to-detect mixes of Debian and local Ruby libraries,
packages **must not** use `rubygems`. In particular the use of
`require 'rubygems'` and `require_gem` is not allowed.

A special exception can be made to this rule when a piece of software
(program/library) is enhanced by the presence of external libraries,
under two conditions:

1.  The library **must not** be necessary for the normal use of the piece
    of software.

2.  The library **must not** be available as a Debian package.

### Unit tests

Upstream unit tests **should** be enabled. When that is the case, a test
failure **must** result in a build failure.

Ruby applications {#applications}
---------------------------------

Except for the package name, which does not have to comply with any
rules, Ruby applications **must** obey all rules that apply to Ruby
libraries (previous sections). In addition, they must obey the following
rules.

### Handling the different Ruby interpreters

If the application is supposed to work correcly with any Ruby
interpreter, then it **must** have a `#!/usr/bin/ruby` shebang, and **must**
depend on `ruby | ruby-interpreter`. This **should** be the default for most
packages, since it will ease the transition to new default versions of
the Ruby interpreter.

If the application is supposed to work with a specific Ruby interpreter,
then it **must** have a `#!/usr/bin/rubyX.Y` shebang and **must** depend on the
specific package for that interpreter. For example, an application that
only works with Ruby 1.9.1 would have a `#!/usr/bin/ruby1.9.1` shebang
and depend on `ruby1.9.1`.

[debian-policy]: http://www.debian.org/doc/debian-policy "The Debian Policy"
[dr-ldo]: debian-ruby@lists.debian.org
